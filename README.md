
Lien github
quentin.gans33@ynov.com  [ELECT-REACT-23] - Rendu [GAUTHIER Donatien]

# YuGiOh CardTracker

Ce projet a pour but de lister toutes les cartes Yu-Gi-Oh, de permettre des recherches pour une carte spécifique, et d'obtenir des détails sur les cartes. Il comprend également un système de "pack opening" et la possibilité de sauvegarder les cartes que l'on souhaite.

De plus, toutes ces fonctionnalités sont liées à mon projet de fin de M2, même s'il utilise une autre API. Cela me permet de m'entraîner.

## Démarrer le projet
- `npm install`
- `npm run dev`

GG ! Rendez-vous sur le lien dans le terminal.

### Package
- **@emotion/react**: ^11.11.1
- **@emotion/styled**: ^11.11.0
- **@mui/material**: ^5.14.19
- **bootstrap**: ^5.3.2
- **react**: ^18.2.0
- **react-bootstrap**: ^2.9.1
- **react-card-flip**: ^1.2.2
- **react-dom**: ^18.2.0
- **react-query**: ^3.39.3
- **react-router-dom**: ^6.20.0

# Acceuil

Sur la page d'accueil, retrouvez la liste complète des cartes Yu-gi-oh, avec une pagination pour une esthétique optimale et une performance accrue, évitant ainsi des temps de chargement excessifs.

Des filtres interagissent entre eux, permettant de croiser les types de cartes et les races. 
Une barre de recherche vous permet de trouver une carte spécifique : elle recherche à chaque lettre une correspondance dans le nom des cartes. 
Si le mot est présent, la carte sera affichée. En cas de correspondance totale, les lettres seront mises en évidence en rouge. 
Pour tester cela, vous pouvez par exemple écrire "White Dragon".

Ps: J'ai voulu filtrer par Set(Deck) mais il y'en a 1102 o_O

### Random Card

Un bouton "Random" dans la barre de navigation vous permet d'y accéder.

Lorsque vous cliquez, une carte se retourne avec un effet visuel particulier sur la première carte. Un bouton "Save Card" devient actif uniquement lorsque la carte est retournée, permettant de la sauvegarder.

Vous devrez recliquer sur la carte pour la remettre face cachée, et ainsi de suite.

### Set

Une page avec la liste des sets de cartes, qui quand on click dessus est censé afficher la liste des cartes correspondantes mais l'api est flingué.

### Deck Buildin

Une liste de carte sur la gauche avec les filtres que l'on peut drag and drop dans la carré deck et qui stock donc les cartes.


### Amélioration

- Connexion, même si pas très utile pour le site :
  -  Un systeme de recupération de cookie sans connexion pour reconnaitre le user ? 
  - Pouvoir sauvegarder le deck

- Trier par set / Liste des cartes par set ( mais bon l'api est bizarre )
- faire un systeme de jeu
- pack opening, démarrer un peu avec les randoms cards



