import {apiGetCard} from "../endpoints/getApi.ts";
import {SetStateAction, useCallback, useEffect, useState} from "react";
import {CardType} from "../types/Card/CardType.ts";
import {CardDisplayName} from "./CardDisplayName";
import {CardDetail} from "./CardDetail.tsx";
import Card from '@mui/material/Card';
import {CardActionArea, CardContent, CardMedia, Typography} from "@mui/material";
import Pagination from '@mui/material/Pagination';
import FilterComponent from "./Filter.tsx";
import "./css/allCard.style.css";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
export const AllCard = ({searchText}) => {
  const [loading, setLoading] = useState(false);
  const [cards, setCards] = useState<CardType[]>();
  const [selectedCard, setSelectedCard] = useState<CardType | null>(null);
  const [isCardDetailOpen, setCardDetailOpen] = useState(false);
  const [selectedType, setSelectedType] = useState("All");
  const [selectedRace, setSelectedRace] = useState("All");
  const [page, setPage] = useState(1);
  const cardsPerPage = 100;

  const fetchCard = useCallback(async () => {
    setLoading(true);
    try {
      const response = await apiGetCard();
      if (response.data) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        setCards(response.data);
        setLoading(false);
      }
    } catch (error) {
      console.error("Error fetching cards:", error);
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchCard();
  }, []);

  const handleOpenCardDetail = (selectedCard: CardType) => {
    setSelectedCard(selectedCard);
    setCardDetailOpen(true);
  };

  const handleCloseCardDetail = () => {
    setCardDetailOpen(false);
  };

  const handlePageChange = (event, value: SetStateAction<number>) => {
    setPage(value);
  };

  const handleTypeChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setSelectedType(event.target.value);
    setPage(1);
  };

  const handleRaceChange = (event: { target: { value: SetStateAction<string>; }; }) => {
    setSelectedRace(event.target.value);
    setPage(1);
  };

  const filteredCards = cards?.filter((card) =>
    card.name.toLowerCase().includes(searchText.toLowerCase()) &&
    (selectedType === "All" || card.type.includes(selectedType)) &&
    (selectedRace === "All" || card.race === selectedRace)
  );

  const indexOfLastCard = page * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = filteredCards?.slice(indexOfFirstCard, indexOfLastCard);

  return (
    <div>
      {loading ? (
        <>Wait...</>
      ) : (
        <div>
          <FilterComponent
            selectedType={selectedType}
            selectedRace={selectedRace}
            onTypeChange={handleTypeChange}
            onRaceChange={handleRaceChange}
          />
          <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center", gap: "20px"}}>
            {isCardDetailOpen && selectedCard && (
              <CardDetail card={selectedCard} show={isCardDetailOpen} onClose={handleCloseCardDetail}/>
            )}
            {currentCards?.length === 0 && (
              <>No Card</>
            )}
            {currentCards?.map((card, index) => (
              <Card key={index} sx={{backgroundColor: "#242424", maxWidth: "150px", textAlign: "center"}} className="cards-item">
                <CardActionArea>
                  {card.card_images && card.card_images.length > 0 && (
                    <CardMedia
                      component="img"
                      src={card.card_images[0].image_url}
                      alt={`Image of ${card.name}`}
                      sx={{width: 150, display: "block", margin: "auto", }}
                      style={{cursor: "pointer"}}
                      onClick={() => handleOpenCardDetail(card)}
                    />
                  )}
                  <CardContent>
                    <Typography gutterBottom component="div"
                                style={{color: "white", fontFamily: "Belwe Bold", fontSize: 11}}>
                      <CardDisplayName card={card} searchText={searchText}/>
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            ))}
          </div>
          {filteredCards && (
            <Pagination
              count={Math.ceil(filteredCards.length / cardsPerPage)}
              page={page}
              onChange={handlePageChange}
              color="primary"
              variant="outlined"
              sx={{marginTop: "20px", display: "flex", justifyContent: "center",}}
              className=""
            />
          )}
        </div>
      )}
    </div>
  );
};

