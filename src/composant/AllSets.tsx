// AllSets.tsx
import React, { SetStateAction, useCallback, useEffect, useState } from "react";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Card,
  CardActionArea,
  CardMedia,
  Typography,
} from "@mui/material";
import { SetType } from "../types/Set/SetType";
import Pagination from "@mui/material/Pagination";
import { SetDisplayName } from "./CardDisplayName";
import SetCardList from "./SetCardList";
import {apiGetSets} from "../endpoints/getApi.ts";

interface AllSetsProps {
  searchText: string;
}

export const AllSets: React.FC<AllSetsProps> = ({ searchText }) => {
  const [loading, setLoading] = useState(false);
  const [sets, setSets] = useState<SetType[]>();
  const [page, setPage] = useState(1);
  const setsPerPage = 50;

  const fetchSets = useCallback(async () => {
    setLoading(true);
    try {
      const response = await apiGetSets();
      if (response) {
        setSets(response);
        setLoading(false);
      }
    } catch (error) {
      console.error("Error fetching sets:", error);
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchSets();
  }, []);

  const handlePageChange = (_event: any, value: SetStateAction<number>) => {
    setPage(value);
  };

  const filteredSets = sets?.filter((set) =>
    set.set_name.toLowerCase().includes(searchText.toLowerCase())
  );

  const indexOfLastSet = page * setsPerPage;
  const indexOfFirstSet = indexOfLastSet - setsPerPage;
  const currentSets = filteredSets?.slice(indexOfFirstSet, indexOfLastSet);

  return (
    <div>
      {loading ? (
        <>Wait...</>
      ) : (
        <div>
          <div style={{ display: "grid", flexWrap: "wrap", gap: "20px", width: "100%" }}>
            {currentSets?.length === 0 && (
              <>No Set</>
            )}
            {currentSets?.map((set, index) => (
              <Accordion key={index} sx={{ backgroundColor: "#242424" }}>
                <AccordionSummary
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Card sx={{ backgroundColor: "#242424" }}>
                    <CardActionArea>
                      {set.set_image && set.set_image.length > 0 && (
                        <CardMedia
                          component="img"
                          src={set.set_image}
                          alt={`Image of ${set.set_name}`}
                          sx={{ width: 80, display: "block", margin: "auto" }}
                          style={{ cursor: "pointer" }}
                        />
                      )}
                    </CardActionArea>
                  </Card>
                  <div style={{ color: "white", margin: "auto" }}>
                    <Typography component="div" style={{ fontFamily: "Belwe Bold" }}>
                      <SetDisplayName set={set} searchText={searchText} />
                    </Typography>
                    <Typography component="div" style={{ fontFamily: "Belwe Bold" }}>
                      Card: {set.num_of_cards}
                    </Typography>
                  </div>
                </AccordionSummary>
                <AccordionDetails>
                  <SetCardList setName={set.set_name}/>
                </AccordionDetails>
              </Accordion>
            ))}
          </div>
          {filteredSets && (
            <Pagination
              count={Math.ceil(filteredSets.length / setsPerPage)}
              page={page}
              onChange={handlePageChange}
              color="primary"
              variant="outlined"
              sx={{ marginTop: "20px", display: "flex", justifyContent: "center" }}
              className=""
            />
          )}
        </div>
      )}
    </div>
  );
};
