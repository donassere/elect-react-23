import {useCallback, useEffect, useState} from "react";
import {apiGetOneCard} from "../endpoints/getApi.ts";
import {CardType} from "../types/Card/CardType.ts";
import Card from '@mui/material/Card';
import {Button, CardContent, CardMedia, List, ListItem, ListItemText, Modal, Typography} from "@mui/material";

export type CardDetailProps = {
  onClose: () => void;
  card: CardType;
  show: boolean;
};

export const CardDetail = ({card, show, onClose}: CardDetailProps) => {
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchTheCard();
  }, [card]);

  const fetchTheCard = useCallback(async () => {
    setLoading(true);
    try {
      const response = await apiGetOneCard(card.id);
      if (response.data) {
        setLoading(false);
      }
    } catch (error) {
      console.error("Error fetching card:", error);
      setLoading(false);
    }
  }, [card]);

  if (!show) {
    return null
  }

  if (loading) {
    return <>Wait..</>
  }

  return (
    <Modal open={show} sx={{maxWidth: "60%", margin: "auto", marginTop: "30px"}}>
      <div style={{ display: "grid" }}>
        <Card sx={{display: 'flex', border: 3, borderColor: "#7a84aa"}}>
          <CardContent sx={{
            flex: 'auto',
            backgroundColor: "#292c42",
            overflowY: 'auto',
            maxHeight: '555px',
          }}>
            <Typography component="div" variant="h3" color="#7a84aa"
                        sx={{fontFamily: "Belwe Bold"}}>{card?.name}</Typography>
            <Typography variant="subtitle1" color="#7a84aa" component="div" sx={{fontFamily: "Belwe Bold"}}>
              Race: {card?.race}
            </Typography>
            <List>
              {card?.card_sets.map((setCard, index) =>
                <ListItem key={index} sx={{ display: "grid"}}>
                  <ListItemText  sx={{fontFamily: "Belwe Bold", color: "#7a84aa"}}>
                    Set Name: {setCard.set_name}
                  </ListItemText>
                  <ListItemText sx={{fontFamily: "Belwe Bold", color: "#7a84aa"}}>
                    Rarity: {setCard.set_rarity}
                  </ListItemText>
                  <ListItemText sx={{fontFamily: "Belwe Bold", color: "#7a84aa"}}>
                    Price: {setCard.set_price}$
                  </ListItemText>
                </ListItem>
              )}
            </List>
          </CardContent>
          {card?.card_images && card.card_images.length > 0 && (
            <CardMedia
              component="img"
              sx={{width: 400}}
              src={card.card_images[0].image_url}
              alt={`Image of ${card.name}`}
            />
          )}
        </Card>
        <Button style={{ marginTop: "50px", backgroundColor: "#7a84aa", color: "#292c42", borderRadius: "20px", padding: "8px 16px", fontFamily: "Belwe Bold"}} onClick={() => onClose()}>Close</Button>
      </div>
    </Modal>
  );
};
