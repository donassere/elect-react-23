import {CardType} from "../types/Card/CardType.ts";
import {ReactElement} from "react";
import {SetType} from "../types/Set/SetType.ts";

export function normalizeLower<Type>(str: Type): string | Type {
  if (typeof str === "string") {
    return str
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "")
      .toLowerCase();
  }
  return str;
}

function colorStartsWith(name: string, search: string) {
  if (!normalizeLower(name).startsWith(normalizeLower(search))) {
    return name;
  }
  return (
    <>
      <span style={{ fontWeight: "bold", color: "#8e0404" }}>{name.substring(0, search.length)}</span>
      {name.substring(search.length)}
    </>
  );
}

export function CardDisplayName({ card, searchText }: {
  card: CardType;
  searchText: string;
}): ReactElement {
  if (searchText) {
    if (
      normalizeLower(
        (card.name ?? ""),
      ).startsWith(normalizeLower(searchText))
    ) {
      return (
        <>
          {colorStartsWith(
            (card.name ?? ""),
            searchText,
          )}
        </>
      );
    }{
      return (
        <>
          {colorStartsWith(
            card.name ?? "",
            searchText.substring(card?.name?.length + 1 || 1),
          )}{" "}
          {colorStartsWith(
            card.name ?? "",
            searchText.substring(0, card?.name?.length || 0),
          )}
        </>
      );
    }
  }
  return (
    <span>
      {(card.name ?? "")}
    </span>
  );
}


export function SetDisplayName({ set, searchText }: {
  set: SetType;
  searchText: string;
}): ReactElement {
  if (searchText) {
    if (
      normalizeLower(
        (set.set_name ?? ""),
      ).startsWith(normalizeLower(searchText))
    ) {
      return (
        <>
          {colorStartsWith(
            (set.set_name ?? ""),
            searchText,
          )}
        </>
      );
    }{
      return (
        <>
          {colorStartsWith(
            set.set_name ?? "",
            searchText.substring(set.set_name?.length + 1 || 1),
          )}{" "}
          {colorStartsWith(
            set.set_name ?? "",
            searchText.substring(0, set.set_name?.length || 0),
          )}
        </>
      );
    }
  }
  return (
    <span>
      {(set.set_name ?? "")}
    </span>
  );
}
