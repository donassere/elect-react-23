import { useDrag } from 'react-dnd';
import React from "react";
import {CardType} from "../../types/Card/CardType.ts";

interface CardProps {
  card: CardType;
}
const Card: React.FC<CardProps> = ({ card }) => {
  const [{ isDragging }, drag] = useDrag({
    type: 'CARD',
    item: { card },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        cursor: 'move',
        marginBottom: '10px',
        padding: '5px',
        borderRadius: '5px',
        maxWidth: '150px',
      }}
    >
      {card?.card_images && card.card_images.length > 0 && (
        <img
          src={card.card_images[0].image_url}
          alt={`Image of ${card.name}`}
          style={{ maxWidth: '170%', maxHeight: '170', width: "70%",
            objectFit: "cover", }}
        />
      )}
    </div>
  );
};

export default Card;
