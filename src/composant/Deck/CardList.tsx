import { useState, useEffect, useCallback } from 'react';
import Card from './Card';
import { apiGetCard } from '../../endpoints/getApi.ts';
import { CardType } from '../../types/Card/CardType.ts';
import FilterComponent from '../Filter.tsx';
import Pagination from '@mui/material/Pagination';

const CardList = () => {
  const [loading, setLoading] = useState(false);
  const [cards, setCards] = useState<CardType[]>();
  const [filterType, setFilterType] = useState('All');
  const [filterRace, setFilterRace] = useState('All');
  const [currentPage, setCurrentPage] = useState(1);
  const cardsPerPage = 50;

  const fetchCard = useCallback(async () => {
    setLoading(true);
    try {
      const response = await apiGetCard();
      if (response && response.data) {
        setCards(response.data);
        setLoading(false);
      }
    } catch (error) {
      console.error('Error fetching cards:', error);
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      fetchCard();
      setLoading(false);
    }, 1000);
  }, [filterType, filterRace]);

  const filteredCards = cards?.filter(
    (card) =>
      (filterType === 'All' || card.type.includes(filterType)) &&
      (filterRace === 'All' || card.race === filterRace)
  );

  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = filteredCards?.slice(indexOfFirstCard, indexOfLastCard);

  const handlePageChange = (_event, value) => {
    setCurrentPage(value);
  };

  return (
    <div style={{ display: 'grid', justifyContent: 'left', marginTop: '-40px', marginLeft: "-40px" }}>
      <FilterComponent
        selectedType={filterType}
        selectedRace={filterRace}
        onTypeChange={(e) => setFilterType(e.target.value)}
        onRaceChange={(e) => setFilterRace(e.target.value)}
      />
      <div
        style={{
          overflowY: 'auto',
          maxHeight: 'calc(100vh - 100px)',
          display: 'flex',
          justifyContent: 'center',
          marginTop: '20px',
        }}
      >
        {loading ? (
          <>Wait...</>
        ) : (
          <div className="card-container">
            {currentCards?.length === 0 && <div>No Card</div>}
            {currentCards?.map((card) => (
              <Card key={card.id} card={card} />
            ))}
          </div>
        )}
      </div>
      <Pagination
        count={Math.ceil((filteredCards?.length || 1) / cardsPerPage)}
        page={currentPage}
        onChange={handlePageChange}
        color="primary"
        sx={{ marginTop: '10px', display: 'flex', justifyContent: 'center' }}
      />
    </div>
  );
};

export default CardList;



