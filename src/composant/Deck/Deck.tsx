import { useDrop } from 'react-dnd';
import { CardType } from '../../types/Card/CardType.ts';
import {useEffect} from "react";
import {CardMedia} from "@mui/material";

const Deck = ({ deck, onDrop }) => {
  const [{ canDrop, isOver }, drop] = useDrop({
    accept: 'CARD',
    drop: (item: CardType) => onDrop(item),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const isActive = canDrop && isOver;

  useEffect(() =>{
    console.log('DEEEEEECK ->', deck);
  }, [deck])

  return (
    <div
      ref={drop}
      style={{
        border: `2px solid ${isActive ? 'green' : '#7a84aa'}`,
        padding: '10px',
        width: '900px',
        minHeight: '600px',
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, minmax(150px, 1fr))',
        gap: '5px',
      }}
    >
      {deck?.map((card, index) => (
        <div key={index} style={{ display: 'flex', width: 150, zIndex: "1" }}>
          {card.card.card_images && card.card.card_images.length > 0 && (
            <CardMedia component="img" src={card.card.card_images[0].image_url} alt={`Image of ${card.name}`} style={{ display: "block", cursor: "pointer", maxWidth: "150px", height: 220, zIndex: "3" }} />
          )}
        </div>
      ))}
    </div>
  );
};

export default Deck;

