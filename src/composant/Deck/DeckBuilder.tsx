import { useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import CardList from './CardList.tsx';
import Deck from './Deck.tsx';
import {CardType} from "../../types/Card/CardType.ts";

const DeckBuilder = () => {
  const [deck, setDeck] = useState<CardType[]>();

  const handleDrop = (card: CardType) => {
    setDeck((prevDeck) => (prevDeck ? [...prevDeck, card] : [card]));
    console.log(deck)
  };


  return (
    <DndProvider backend={HTML5Backend}>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '10px', flex: '1', maxWidth: '300px' }}>
          <CardList />
        </div>

        <div style={{ flex: '2', minWidth: '400px' }}>
          <>Drag card here</>
          <Deck deck={deck || []} onDrop={(card: CardType) => handleDrop(card)} />
        </div>
      </div>
    </DndProvider>
  );
};

export default DeckBuilder;
