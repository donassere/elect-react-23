// FilterComponent.jsx
import {MenuItem, Select} from "@mui/material";
import "./css/filter.style.css";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
const FilterComponent = ({selectedType, selectedRace, onTypeChange, onRaceChange}) => {
  const allRaces = [
    "All",
    "Aqua",
    "Beast",
    "Beast-Warrior",
    "Creator-God",
    "Cyberse",
    "Dinosaur",
    "Divine-Beast",
    "Dragon",
    "Fairy",
    "Fiend",
    "Fish",
    "Insect",
    "Machine",
    "Plant",
    "Psychic",
    "Pyro",
    "Reptile",
    "Rock",
    "Sea Serpent",
    "Spellcaster",
    "Thunder",
    "Warrior",
    "Winged Beast",
    "Wyrm",
    "Zombie",
    "Field",
    "Equip",
    "Continuous",
    "Quick-Play",
    "Ritual",
    "Counter",
  ];

  const allTypes = [
    "All",
    "Effect Monster",
    "Flip Effect Monster",
    "Flip Tuner Effect Monster",
    "Gemini Monster",
    "Normal Monster",
    "Normal Tuner Monster",
    "Pendulum Effect Monster",
    "Pendulum Effect Ritual Monster",
    "Pendulum Flip Effect Monster",
    "Pendulum Normal Monster",
    "Pendulum Tuner Effect Monster",
    "Ritual Effect Monster",
    "Ritual Monster",
    "Spell Card",
    "Spirit Monster",
    "Toon Monster",
    "Trap Card",
    "Tuner Monster",
    "Union Effect Monster",
    "Fusion Monster",
    "Link Monster",
    "Pendulum Effect Fusion Monster",
    "Synchro Monster",
    "Synchro Pendulum Effect Monster",
    "Synchro Tuner Monster",
    "XYZ Monster",
    "XYZ Pendulum Effect Monster",
    "Skill Card",
    "Token",
  ];

  return (
    <div style={{display: "flex", justifyContent: "center", gap: "20px", marginTop: "20px", marginBottom: "30px"}}>
      <Select
        value={selectedType}
        onChange={onTypeChange}
        displayEmpty
        inputProps={{'aria-label': 'Without label'}}
        sx={{color: "#f9f9f9"}}
      >
        {allTypes.map((type) => (
          <MenuItem key={type} value={type} sx={{ color: "#242424"}}>
            {type}
          </MenuItem>
        ))}
      </Select>
      <Select
        value={selectedRace}
        onChange={onRaceChange}
        displayEmpty
        inputProps={{'aria-label': 'Without label'}}
        sx={{color: "#f9f9f9"}}
      >
        {allRaces.map((race) => (
          <MenuItem key={race} value={race} sx={{ color: "#242424"}}>
            {race}
          </MenuItem>
        ))}
      </Select>
    </div>
  );
};

export default FilterComponent;
