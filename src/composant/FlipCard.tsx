import { useCallback, useState } from "react";
import { apiGetRandomCard } from "../endpoints/getApi.ts";
import { CardType } from "../types/Card/CardType.ts";
import ReactCardFlip from "react-card-flip";
import Card from "@mui/material/Card";
import { CardMedia, Button, Typography } from "@mui/material";
import BackCard from "../assets/BackCardYugi.jpg";

export const FlipCard = () => {
  const [isFlipped, setFlipped] = useState(false);
  const [card, setCard] = useState<CardType | null>(null);
  const [savedCards, setSavedCards] = useState<CardType[]>([]);

  const fetchRandomCard = useCallback(async () => {
    try {
      const response = await apiGetRandomCard();
      if (response) {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        setCard(response);
      }
    } catch (error) {
      console.error("Error fetching card:", error);
    }
  }, []);

  const loadImageAndFlip = () => {
    const image = new Image();
    image.src = card?.card_images[0].image_url || "";
  };

  const handleClick = () => {
    fetchRandomCard();
    loadImageAndFlip()
    setFlipped(true)
  };

  const handleClick2 = () => {
    setFlipped(false);
  };

  const handleSaveCard = () => {
    if (card) {
      setSavedCards((prevSavedCards) => [...prevSavedCards, card]);
    }
  };

  return (
    <div>
      <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal" containerStyle={{ maxWidth: 300, justifyContent: "center", display: "static", marginLeft: "auto", marginRight: "auto"}}>
        <Card onClick={handleClick}>
          <CardMedia component="img" sx={{ width: 300 }} src={BackCard} alt="Back Card" />
        </Card>
        <Card onClick={handleClick2}>
          <CardMedia
            component="img"
            sx={{ width: 300 }}
            src={card?.card_images[0].image_url}
            alt={`Image of ${card?.name}`}
          />
        </Card>
      </ReactCardFlip>

      <Button onClick={handleSaveCard} variant="contained" color="primary" sx={{ marginTop: 2 }} disabled={!isFlipped}>
        Save Card
      </Button>

      {savedCards.length > 0 && (
        <div>
          <Typography variant="h5" sx={{ marginTop: 2 }}>
            Saved Cards:
          </Typography>
          <div style={{ display: "flex"}}>
            {savedCards.map((savedCard, index) => (
              <Card key={index} sx={{ width: 150, margin: 1}}>
                <CardMedia
                  component="img"
                  sx={{ height: 220 }}
                  src={savedCard.card_images[0].image_url}
                  alt={`Image of ${savedCard.name}`}
                />
              </Card>
            ))}
          </div>
        </div>
      )}
    </div>
  );
};

