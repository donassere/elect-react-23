// NavBar.tsx
import yugiLogo from "../assets/Yugi.png";
import { SetStateAction, useState} from "react";
import "./css/navbar.style.css";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
const NavBar = ({onSearchChange}) => {
  const [searchText, setSearchText] = useState("");

  const handleSearchChange = (e: { target: { value: SetStateAction<string>; }; }) => {
    setSearchText(e.target.value);
    onSearchChange(e.target.value);
  };

  const handleRandom = () => {
    window.location.href = "/random";
  };

  const handleDeck = () => {
    window.location.href = "/deck";
  };

  const handleSets = () => {
    window.location.href = "/sets";
  };
  const handleHome = () => {
    window.location.href = "/";
  };

  return (
    <nav className="navbar">
      <div className="logo-container">
        <a href="../assets/Yugi.png" target="_blank">
          <img src={yugiLogo} className="logo" alt="Yugi logo" onClick={handleHome} />
        </a>
      </div>
      <div className="search-container">
        <input
          type="text"
          placeholder="Search by card name..."
          value={searchText}
          onChange={handleSearchChange}
          className="search-input"
        />
      </div>
      <div className="button-container">
        <button onClick={handleRandom} className="pack-open-button">
          Random
        </button>
      </div>
      <div className="button-container">
        <button onClick={handleSets} className="pack-open-button">
          Sets
        </button>
      </div>
      <div className="button-container">
        <button onClick={handleDeck} className="pack-open-button">
          Deck Builder
        </button>
      </div>
    </nav>
  );
};

export default NavBar;
