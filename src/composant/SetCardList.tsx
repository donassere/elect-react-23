// SetCardList.tsx
import React, { useEffect, useState } from 'react';
import { CardType } from '../types/Card/CardType.ts';
import { apiGetCardsInSet } from '../endpoints/getApi.ts';
import { List, ListItem, ListItemText } from '@mui/material';

interface SetCardListProps {
  setName: string;
}

const SetCardList: React.FC<SetCardListProps> = ({ setName }) => {
  const [cards, setCards] = useState<CardType[]>([]);

  useEffect(() => {
    const fetchCardsInSet = async () => {
      try {
        const response = await apiGetCardsInSet(setName);
        if (response) {
          setCards(response);
        }
      } catch (error) {
        console.error('Error fetching cards in set:', error);
      }
    };

    fetchCardsInSet();
  }, [setName]);

  return (
    <List>
      {cards.map((card, cardIndex) => (
        <ListItem key={cardIndex} sx={{ display: "grid" }}>
          <ListItemText sx={{ fontFamily: "Belwe Bold", color: "#7a84aa" }}>
            Card Name: {card.name}
          </ListItemText>
        </ListItem>
      ))}
    </List>
  );
};

export default SetCardList;
