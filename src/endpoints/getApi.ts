import {CardType} from "../types/Card/CardType.ts";
import {SetType} from "../types/Set/SetType.ts";

export type ApiRequestReturnType<
  InnerType,
  ErrorType = Record<never, never> | null,
> = {
  response: Response | null;
} & ({ data: InnerType; ok: true } | { data: ErrorType; ok: false });

export async function apiGetCard(): Promise<
  ApiRequestReturnType<CardType[]>> {
  try {
    const response = await fetch(
      "https://db.ygoprodeck.com/api/v7/cardinfo.php",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des cartes:", error);
    throw error;
  }
}

export async function apiGetOneCard(id: number): Promise<
  ApiRequestReturnType<CardType>> {
  try {
    const response = await fetch(
      `https://db.ygoprodeck.com/api/v7/cardinfo.php?id=${id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des cartes:", error);
    throw error;
  }
}

export async function apiGetRandomCard(): Promise<
  ApiRequestReturnType<CardType>> {
  try {
    const response = await fetch(
      "https://db.ygoprodeck.com/api/v7/randomcard.php",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des cartes:", error);
    throw error;
  }
}

export async function apiGetSets(): Promise<
  ApiRequestReturnType<SetType[]>> {
  try {
    const response = await fetch(
      "https://db.ygoprodeck.com/api/v7/cardsets.php",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des sets:", error);
    throw error;
  }
}

export async function apiGetCardsInSet(set_name: string): Promise<
  ApiRequestReturnType<CardType>> {
  try {
    const response = await fetch(
      `https://db.ygoprodeck.com/api/v7/cardsetsinfo.php?setname=${set_name}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return await response.json();
  } catch (error) {
    console.error("Erreur lors de la récupération des cartes:", error);
    throw error;
  }
}
