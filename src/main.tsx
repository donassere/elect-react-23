import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import App from "./pages/App";
import Sets from "./pages/Sets.tsx";
import Random from "./pages/Random.tsx";
import BuildDeck from "./pages/BuildDeck.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/random",
    element: <Random />,
  },
  {
    path: "/deck",
    element: <BuildDeck />,
  },
  {
    path: "/sets",
    element: <Sets />,
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
