// App.tsx
import './App.css'
import { AllCard } from "../composant/AllCard.tsx";
import NavBar from "../composant/NavBar.tsx";
import {SetStateAction, useState} from "react";

function App() {
  const [searchText, setSearchText] = useState("");

  const handleSearchChange = (value: SetStateAction<string>) => {
    setSearchText(value);
  };

  return (
    <>
      <NavBar onSearchChange={handleSearchChange} />
      <div className="card">
        <AllCard searchText={searchText} />
      </div>
    </>
  );
}

export default App;

