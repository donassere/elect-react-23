// BuildDeck.jsx

import DeckBuilder from "../composant/Deck/DeckBuilder.tsx";

const App = () => {
  return (
    <div>
      <h1>Deck Builder</h1>
      <DeckBuilder />
    </div>
  );
};

export default App;
