// Sets.tsx
import './App.css'
import NavBar from "../composant/NavBar.tsx";
import {SetStateAction, useState} from "react";
import {AllSets} from "../composant/AllSets.tsx";

function App() {
  const [searchText, setSearchText] = useState("");

  const handleSearchChange = (value: SetStateAction<string>) => {
    setSearchText(value);
  };

  return (
    <>
      <NavBar onSearchChange={handleSearchChange} />
      <div className="card">
        <AllSets searchText={searchText} />
      </div>
    </>
  );
}

export default App;
