export interface CardImagesType {
  id: number,
  image_url: string,
  image_url_small: string,
  image_url_cropped: string,
}
