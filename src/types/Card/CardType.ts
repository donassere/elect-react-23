import {CardSetsType} from "./CardSetsType.ts";
import {CardImagesType} from "./CardImagesType.ts";
import {CardPricesType} from "./CardPricesType.ts";

export interface CardType {
  id: number,
  name: string,
  type: string,
  frameType: string,
  desc: string,
  atk?: number,
  def?: number,
  level?: number,
  race: string,
  archeType: string,
  attribute?: string
  card_sets: CardSetsType[],
  card_images: CardImagesType[],
  card_prices: CardPricesType[],
}
